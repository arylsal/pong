import turtle


class Turtle(object):

    def __init__(self, position_x, position_y, color):
        self.position_x = position_x
        self.position_y = position_y
        self.color = color
        self.tur = turtle.Turtle()

    def setup(self):
        self.tur.speed(0)
        self.tur.shape("square")
        self.tur.color(self.color)


class Cube(Turtle):

    def __init__(self, position_x, position_y, color, shape_width, shape_length):
        super().__init__(position_x, position_y, color)
        self.shape_width = shape_width
        self.shape_length = shape_length

    def setup(self):
        super().setup()
        self.tur.shapesize(stretch_wid=self.shape_width, stretch_len=self.shape_length)
        self.tur.penup()
        self.tur.goto(self.position_x, self.position_y)


class Player(Cube):
    movement_units = 20

    def __init__(self, position_x, position_y, color, shape_width, shape_length):
        super().__init__(position_x, position_y, color, shape_width, shape_length)

    def move(self, direction):
        if direction == "down":
            if self.tur.ycor() > -240:
                self.tur.sety(self.tur.ycor() - self.movement_units)
        if direction == "up":
            if self.tur.ycor() < 240:
                self.tur.sety(self.tur.ycor() + self.movement_units)


class Ball(Cube):

    def __init__(self, position_x, position_y, color, shape_width, shape_length, dx, dy):
        super().__init__(position_x, position_y, color, shape_width, shape_length)
        self.dx = dx
        self.dy = dy

    def move(self):
        self.tur.setx(self.tur.xcor() + self.dx)
        self.tur.sety(self.tur.ycor() + self.dy)

    def check_boarder_collision_y(self):
        if self.tur.ycor() > 290:
            self.tur.sety(290)
            self.dy *= -1

        elif self.tur.ycor() < -290:
            self.tur.sety(-290)
            self.dy *= -1

    def check_boarder_collision_x(self):
        if self.tur.xcor() > 350:
            self.tur.goto(0, 0)
            self.dx *= -1

        elif self.tur.xcor() < -350:
            self.tur.goto(0, 0)
            self.dx *= -1

    def check_for_player_collision_left(self, player):
        if self.tur.xcor() < -340 and player.tur.ycor() + 50 > self.tur.ycor() > player.tur.ycor() - 50:
            self.dx *= -1

    def check_for_player_collision_right(self, player):
        if self.tur.xcor() > 340 and player.tur.ycor() + 50 > self.tur.ycor() > player.tur.ycor() - 50:
            self.dx *= -1


class AIPlayer(Player):

    def auto_move(self, ball):
        if self.tur.ycor() < ball.tur.ycor() and abs(self.tur.ycor() - ball.tur.ycor()) > 20:
            self.move("up")
        elif self.tur.ycor() > ball.tur.ycor() and abs(self.tur.ycor() - ball.tur.ycor()) > 20:
            self.move("down")


class Message(Turtle):

    def write(self, message):
        self.tur.penup()
        self.tur.hideturtle()
        self.tur.goto(self.position_x, self.position_y)
        self.tur.write(message, align="center", font=("Courier", 24, "normal"))


def main():
    screen_width = 800
    screen_height = 600

    wn = turtle.Screen()
    wn.title("Pong")
    wn.bgcolor("black")
    wn.setup(screen_width, screen_height)
    wn.tracer(0)

    m1 = Message(0, -250, "white")
    m1.setup()
    m1.write("PONG")

    p1 = Player(-350, 0, "white", 5, 1)
    p1.setup()

    a1 = AIPlayer(350, 0, "white", 5, 1)
    a1.setup()

    b1 = Ball(0, 0, "white", 1, 1, 0.2, 0.2)
    b1.setup()

    wn.listen()
    wn.onkeypress(lambda: p1.move("up"), "w")
    wn.onkeypress(lambda: p1.move("down"), "s")

    while True:
        b1.move()
        b1.check_boarder_collision_x()
        b1.check_boarder_collision_y()

        b1.check_for_player_collision_right(a1)
        b1.check_for_player_collision_left(p1)

        a1.auto_move(b1)
        wn.update()


main()
